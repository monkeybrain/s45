import {useState} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'

export default function CourseView (){

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)

	return(
		<Container className = "mt-5">
			<Row>
				<Col lg = {{span: 6, offset: 3}}>
					<Card>
						<Card.Body className ="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>5:30pm to 9:30pm</Card.Text>
							<Button variant = "primary"> Enroll</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)
}