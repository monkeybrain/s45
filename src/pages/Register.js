import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register() {


		const {user,setUser} = useContext(UserContext);
		// State hooks to store the values of the input fields
		const [email, setEmail] = useState('');
	    const [password1, setPassword1] = useState('');
	    const [password2, setPassword2] = useState('');
	    // State to determine whether submit button is enabled or not
	    const [isActive, setIsActive] = useState('');

	    // Check if values are successfully binded
    	console.log(email);
   		console.log(password1);
   		console.log(password2);


   		function registerUser(e){

   			//Prevents page redirection via form submission
   			e.preventDefault();

   			localStorage.setItem('email', email);
   			localStorage.setItem('password1', password1);

   			setUser({
		        email:localStorage.getItem('email'),
		        password1:localStorage.getItem('password1')
		    })

   			//Clears the input fields
   			setEmail('');
   			setPassword1('');
   			setPassword2('');

   			alert('You have successfully registered!');
   		}




		useEffect(() => {

	       
	        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
	            setIsActive(true);
	        } else {
	            setIsActive(false);
	        }

	    }, [email, password1, password2]);


		    return (
		    	(user.id!==null) ?
		    	<Redirect to='/courses'/>
		    	:
		        <Form onSubmit={(e)=>registerUser(e)}>
		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email"
			         		value={email} 
			                onChange={e => setEmail(e.target.value)} 
			                required
		                />
		                <Form.Text className="text-muted">
		                    Don't worry your password is safe with us
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value={password1} 
			                onChange={e => setPassword1(e.target.value)} 
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="password2">
		                <Form.Label>Verify Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Verify Password"
			                value={password2} 
			                onChange={e => setPassword2(e.target.value)} 
			                required
		                />
		            </Form.Group>

 					{/* conditionally render submit button based on isActive state */}
            	    { isActive ? 
            	    	<Button variant="primary" type="submit" id="submitBtn">
            	    		Submit
            	    	</Button>
            	        : 
            	        <Button variant="danger" type="submit" id="submitBtn" disabled>
            	        	Submit
            	        </Button>
            	    }

		        </Form>
		    )

		}
