import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Page Not Found",
        content: "Page Cannot be Found",
        destination: "/",
        label: "Go home"
    }
    
    return (
        <Banner data={data}/>
    )
}
