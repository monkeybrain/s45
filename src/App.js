// import {Fragment} from 'react'
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
import CourseView from './pages/CourseView'
import Courses from './pages/Courses'
import ErrorPage from './pages/ErrorPage'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'

import './App.css';
import {UserProvider} from './UserContext';

function App() {

  
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
  	console.log(user);
  	console.log(localStorage);
  }, [user])


  /*
  the userprovider component is what allows other components to consume or use our context. any component which is not wrapped by userprovider wil not have access to the values provided for our context

  you can pass data or information to our context by providing a "value " attribute in our userprovider. data passed here by ither components by unwrapping our contezt using usecontext hook. 

  */

  return (
  <UserProvider value = {{user, setUser, unsetUser}}>
     <Router>
     	<AppNavbar/>
     	<Container>
     		<Switch>
     			<Route exact path = "/" component = {Home}/>
     			<Route exact path = "/courses" component = {Courses}/>
     			<Route exact path = "/courseView" component = {<courseView/>}/>
     			<Route exact path = "/login" component = {Login}/>
     			<Route exact path = "/register" component = {Register}/>
     			<Route exact path = "/logout" component = {Logout}/>
          <Route exact path = "/" component = {Logout}/>
          <Route path = "*" component = {ErrorPage} />
     		</Switch>
     	</Container>
   </Router>
  </UserProvider>
  );
}

export default App;

